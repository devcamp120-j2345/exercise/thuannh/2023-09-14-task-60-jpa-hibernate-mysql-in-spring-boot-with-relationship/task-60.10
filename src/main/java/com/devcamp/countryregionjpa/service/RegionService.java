package com.devcamp.countryregionjpa.service;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


import com.devcamp.countryregionjpa.model.CRegion;
import com.devcamp.countryregionjpa.repository.IRegionRepository;

import org.springframework.stereotype.Service;

@Service
public class RegionService {
    @Autowired
	IRegionRepository pRegionRepository;

    public ArrayList<CRegion> getAllRegions() {
        ArrayList<CRegion> pRegions = new ArrayList<CRegion>();

		pRegionRepository.findAll().forEach(pRegions::add);

        return pRegions;
    }
}
