package com.devcamp.countryregionjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.countryregionjpa.model.CCountry;

public interface ICountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);
}