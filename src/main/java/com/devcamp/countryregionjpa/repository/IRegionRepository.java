package com.devcamp.countryregionjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.countryregionjpa.model.CRegion;

public interface IRegionRepository extends JpaRepository<CRegion, Long> {

}
